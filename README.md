# Task Manager

## DEVELOPER INFO

* NAME: Kolesnik Nikolay

* E-MAIL: kolesnik.nik.vrn@gmail.com

## SOFTWARE

* OpenJDK 8

* Intellij Idea

* MS Windows 10

## HARDWARE

* **RAM**: 16Gb

* **CPU**: i5

* **HDD**: 128Gb

## BUILD APPLICATION

```shell
mvn clean install
```

## RUN APPLICATION

```shell
java -jar "Task Manager.jar"
```
