package ru.t1consulting.nkolesnik.tm.command.system;

import ru.t1consulting.nkolesnik.tm.api.service.ICommandService;
import ru.t1consulting.nkolesnik.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
