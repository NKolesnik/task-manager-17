package ru.t1consulting.nkolesnik.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void clearProjectList();

    void createProject();

    void showProjectByIndex();

    void showProjectById();

    void updateProjectByIndex();

    void updateProjectById();

    void removeProjectByIndex();

    void removeProjectById();

    void changeProjectStatusByIndex();

    void changeProjectStatusById();

    void startProjectByIndex();

    void startProjectById();

    void completeProjectByIndex();

    void completeProjectById();

}
