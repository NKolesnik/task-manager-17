package ru.t1consulting.nkolesnik.tm.api.service;

public interface IServiceLocator {

    ILoggerService getLoggerService();

    IProjectService getProjectService();

    ITaskService getTaskService();

    IProjectTaskService getProjectTaskService();

    ICommandService getCommandService();

}
