package ru.t1consulting.nkolesnik.tm.api.controller;

public interface ITaskController {

    void showTaskList();

    void clearTaskList();

    void createTask();

    void showTaskByIndex();

    void showTaskListByProjectId();

    void showTaskById();

    void updateTaskByIndex();

    void updateTaskById();

    void removeTaskByIndex();

    void removeTaskById();

    void changeTaskStatusByIndex();

    void changeTaskStatusById();

    void startTaskByIndex();

    void startTaskById();

    void completeTaskByIndex();

    void completeTaskById();

}
