package ru.t1consulting.nkolesnik.tm.api.model;

public interface ICommand {

    String getName();

    String getDescription();

    String getArgument();

}
